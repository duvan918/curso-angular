import {v4 as uuid} from 'uuid';

export class DestinoViaje {
    private selected!: boolean;
    servicios: string[];
    id = uuid();

    constructor(public nombre: string, public url: string) {
        this.servicios = ['piscina', 'desayuno'];
    }

    isSelected(): boolean {
        return this.selected;
    }

    setSelected(s: boolean) {
        this.selected = s;
    }
}
