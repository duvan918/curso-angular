import { Component, OnInit, EventEmitter, Output} from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.models';
import { DestinosApiClient } from './../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;

  constructor(public destinosApiClient:DestinosApiClient) {
    this.onItemAdded = new EventEmitter();
  }

  ngOnInit(): void {
  }

  guardar(nombre: string, url: string): boolean {
    let d = new DestinoViaje(nombre, url)
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    return false;
  }

  // agregado(d: DestinoViaje) {
  //   this.destinos.push(d);
  // }

  elegido(e:DestinoViaje) {
    this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    e.setSelected(true);
  }
}
