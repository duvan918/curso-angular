//Instalar typescript de forma global
npm install -g typescript

//Angular CLI de forma global
npm install -g @angular/cli

//Crear y configurar llave ssh
cd ~
ssh-keygen
mkdir ~/.ssh
ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts

//Crear app angular
ng new angular-hola-mundo

//Servir
ng serve

//Crear componente
ng generate component saludador

//Agregar bootstrap
npm i -s bootstrap

//Iterar
*ngFor

//Link github curso profesor
https://github.com/asmx1986/fswd-angular6

//Herramientas
ng build --target = production --base-href /
now
npm install -g tsun 

